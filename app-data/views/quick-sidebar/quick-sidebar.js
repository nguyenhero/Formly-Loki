﻿// 'use strict';
/* SAVIS Vietnam Corporation
*
* This module is to separate configuration of authentication to app.js
* NguyenBv - Jan 2017
*/

define(['jquery', 'angularAMD', 'components/service/amdservice', 'Metronic.QuickSidebar'], function (jQuery, angularAMD) {
    'use strict';
    /* Init the auth config, interceptor, and runtime modules */
    var factory = {};

    factory.init = function (app) {
        app.controller('QuickSidebarCtrl', ['$scope', '$timeout', '$rootScope', '$location', '$route', '$http', 'constantsAMD', '$log', '$translate',
            function ($scope, $timeout, $rootScope, $location, $route, $http, constantsAMD, $log, $translate) {
                $timeout(function () {
                    QuickSidebar.init();
                }, 2000);
            }
        ]);

    };

    return factory;
});