﻿/* SAVIS Vietnam Corporation
 *
 * This module is to separate configuration of authentication to app.js
 * NguyenBv - Jan 2017
 */

define(['angularAMD', 'jquery',
    'slimscroll', 'components/service/amdservice', 'Metronic.Layout', 'jquery-ui'
], function (angularAMD, jQuery) {
    'use strict';
    /* Init the auth config, interceptor, and runtime modules */
    var factory = {};
    factory.init = function (app) {
        app.controller('HeaderCtrl', ['$scope', '$timeout', '$route', '$interval', '$translate', 'tmhDynamicLocale', 'Notifications', '$rootScope', 'constantsAMD',
         '$location', '$http', '$log', '$uibModal',
            function ($scope, $timeout, $route, $interval, $translate, tmhDynamicLocale, notifications, $rootScope, constantsAMD, 
                $location, $http, $log, $uibModal) {
                if (window.Notification) {
                    Notification.requestPermission();
                }
                /* Notification */
                $scope.Notifications = notifications;
                $scope.closeAlert = function (item) {
                    notifications.pop(item);
                };
                $scope.success = function (title, message) {
                    constantsAMD.setNotification(notifications, 'info-default', title, message);
                };
                $scope.error = function (title, message) {
                    constantsAMD.setNotification(notifications, 'danger', title, message);
                };
                $scope.success = function (message) {
                    constantsAMD.setNotification(notifications, 'info-default', 'Thông báo', message);
                };
                $scope.error = function (message) {
                    constantsAMD.setNotification(notifications, 'danger', 'Thông báo', message);
                };
                /*------------------------------------------------------------------**/
                Layout.initHeader(); // init header
                $scope.appVersion = angularAMD.Version; // Application version 

                 
 
                // Logout function
                $scope.LogOut = function () { 
                };

                //Multi language
                $scope.ListLanguage = [{
                    Name: "Việt Nam",
                    Key: "vi",
                    IconSrc: "../assets/global/img/flags/vn.png"
                }, {
                    Name: "English",
                    Key: "en",
                    IconSrc: "../assets/global/img/flags/us.png"
                }, {
                    Name: "France",
                    Key: "fr",
                    IconSrc: "../assets/global/img/flags/fr.png"
                }];
                $scope.SelectedLanguage = {
                    Name: "Việt Nam",
                    Key: "vi",
                    IconSrc: "../assets/global/img/flags/vn.png"
                }
                var curentLang = $translate.use();
                if (typeof (curentLang) === "undefined") {
                    curentLang = 'vi';
                };
                tmhDynamicLocale.set(curentLang);
                angular.forEach($scope.ListLanguage, function (lang) {
                    if (lang.Key === curentLang) {
                        $scope.SelectedLanguage = lang;
                    }
                });
                $scope.SelectLanguage = function (lang) {
                    $scope.SelectedLanguage = lang;
                    $translate.use(lang.Key);
                    tmhDynamicLocale.set(lang.Key);
                };
            }
        ]);
    };

    return factory;
});